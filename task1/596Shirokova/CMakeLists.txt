set(SRC_FILES
    Main.h
    Main.cpp
    common/Application.hpp
    common/Application.cpp
    common/Camera.cpp
    common/Camera.hpp
    common/Common.h
    common/Mesh.hpp
    common/Mesh.cpp
    common/ShaderProgram.cpp
    common/ShaderProgram.hpp
    common/DebugOutput.h
    common/DebugOutput.cpp
)
set(SHADER_FILES
        596ShirokovaData1/color.vert
        596ShirokovaData1/color.frag
)

set(TXT_FILES
        596ShirokovaData1/coordinates.txt
)

source_group("Shaders" FILES ${SHADER_FILES}) 
source_group("TXT" FILES ${TXT_FILES})

include_directories(common)

MAKE_OPENGL_TASK(596Shirokova 1 "${SRC_FILES}")

if (UNIX)
    target_link_libraries(596Shirokova1 stdc++fs)
endif()


