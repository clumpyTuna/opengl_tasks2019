#include "main.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"

#include <iostream>
#include <vector>

#include <random>
#include <fstream>
#include <cmath>

std::vector<std::vector<float>> ReadTerrain() {
    std::vector<std::vector<float>> terrain(512, std::vector<float>(512, 0));

    std::string file_path = "./591DurasovData1/terrain.txt";
    std::fstream fs;
    fs.open(file_path, std::fstream::in);
    for (int i = 0; i < 512; i++) {
        for (int j = 0; j < 512; j++) {
            float value;
            fs >> value;
            terrain[i][j] = value + 20 * pow(value, 3);

        }
    }
    fs.close();

    return terrain;
}

MeshPtr makeSurface(unsigned int N = 100, int size = 5, int h = 10)
{

    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> norm;

    int sz = 511;
    std::vector<std::vector<float>> values = ReadTerrain();

    for (int i = 0; i < sz; i++) {
        for (int j = 0; j < sz; j++) {


            // first triangle
            glm::vec3 x(i, j, values[i][j]);
            glm::vec3 y(i + 1, j, values[i + 1][j]);
            glm::vec3 z(i, j + 1, values[i][j + 1]);

            vertices.push_back(x);
            vertices.push_back(y);
            vertices.push_back(z);

            norm.push_back(glm::normalize(x));
            norm.push_back(glm::normalize(y));
            norm.push_back(glm::normalize(z));

            // second triangle
            x = glm::vec3(i + 1, j + 1, values[i + 1][j + 1]);
            y = glm::vec3(i + 1, j, values[i + 1][j]);
            z = glm::vec3(i, j + 1, values[i][j + 1]);

            vertices.push_back(x);
            vertices.push_back(y);
            vertices.push_back(z);

            norm.push_back(glm::normalize(x));
            norm.push_back(glm::normalize(y));
            norm.push_back(glm::normalize(z));
        }
    }

    // set mesh
    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(norm.size() * sizeof(float) * 3, norm.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);

    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}


class SampleApplication : public Application
{
public:
    MeshPtr _surface;
    ShaderProgramPtr _shader;

    void makeScene() override
    {
        Application::makeScene();

        _surface = makeSurface();
        _surface->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(-20.0f, -20.0f, -20.0f)));

        _shader = std::make_shared<ShaderProgram>("591DurasovData1/simple.vert", "591DurasovData1/simple.frag");
        _cameraMover = std::make_shared<FreeCameraMover>();
    }

    void draw() override
    {
        Application::draw();

        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        _shader->use();

        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        _shader->setMat4Uniform("modelMatrix", _surface->modelMatrix());
        _surface->draw();
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}